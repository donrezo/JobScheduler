package scheduler

import (
	"sync/atomic"
	"time"
)

type Scheduler struct {
	jobQueue     chan IJob
	stop         chan struct{}
	timedJobs    *JobTimer
	pendingJobs  int32
	shuttingDown int32
}

func NewJobScheduler(queueSize int) *Scheduler {
	js := &Scheduler{
		jobQueue:  make(chan IJob, queueSize),
		stop:      make(chan struct{}),
		timedJobs: NewJobTimer(),
	}
	go js.run()
	return js
}

func (js *Scheduler) run() {
	for {
		select {
		case job := <-js.jobQueue:
			job.Execute()
			atomic.AddInt32(&js.pendingJobs, -1)
		case <-js.stop:
			close(js.jobQueue) // Close the job queue to prevent new submissions
			js.flushRemainingJobs()
			return
		}
	}
}

func (js *Scheduler) flushRemainingJobs() {
	for job := range js.jobQueue {
		job.Execute()
		atomic.AddInt32(&js.pendingJobs, -1)
	}
}

func (js *Scheduler) Push(job IJob) {
	if atomic.LoadInt32(&js.shuttingDown) == 1 {
		return // Reject new jobs if shutting down
	}
	atomic.AddInt32(&js.pendingJobs, 1)
	js.jobQueue <- job
}

func (js *Scheduler) Flush() {
	for atomic.LoadInt32(&js.pendingJobs) > 0 {
		time.Sleep(time.Millisecond * 10) // Wait for the jobs to finish
	}
}

func (js *Scheduler) Shutdown() {
	atomic.StoreInt32(&js.shuttingDown, 1)

	// Shut down the timed job scheduler first
	js.timedJobs.Shutdown()

	// Then send shutdown signal to stop the scheduler
	js.stop <- struct{}{}

	// Optionally, wait for all jobs to be flushed
	js.Flush()
}

func (js *Scheduler) PushAfterJob(action func(), tickAfter int) {
	job := NewJob(action)
	js.timedJobs.Push(job, tickAfter)
}

func (js *Scheduler) PushAfterJobWithOneArg(action func(interface{}), arg1 interface{}, tickAfter int) {
	job := NewJobWithOneArg(action, arg1)
	js.timedJobs.Push(job, tickAfter)
}

func (js *Scheduler) PushAfterJobWithTwoArgs(action func(interface{}, interface{}), arg1 interface{}, arg2 interface{}, tickAfter int) {
	job := NewJobWithTwoArgs(action, arg1, arg2)
	js.timedJobs.Push(job, tickAfter)
}

func (js *Scheduler) PushAfterJobWithThreeArgs(action func(interface{}, interface{}, interface{}), arg1 interface{}, arg2 interface{}, arg3 interface{}, tickAfter int) {
	job := NewJobWithThreeArgs(action, arg1, arg2, arg3)
	js.timedJobs.Push(job, tickAfter)
}
