package scheduler

import (
	"sync/atomic"
	"time"
)

type JobTimerElement struct {
	execTick int64
	job      IJob
}

func (e JobTimerElement) Less(other JobTimerElement) bool {
	return e.execTick < other.execTick
}

type JobTimer struct {
	priorityQueue *PriorityQueue[JobTimerElement]
	stop          chan struct{}
	shuttingDown  int32
}

func NewJobTimer() *JobTimer {
	jt := &JobTimer{
		priorityQueue: NewPriorityQueue[JobTimerElement](),
		stop:          make(chan struct{}),
	}
	go jt.run()
	return jt
}

func (jt *JobTimer) run() {
	ticker := time.NewTicker(1 * time.Millisecond) // Adjust ticker frequency as needed
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			jt.flush(time.Now().UnixNano())
		case <-jt.stop:
			return
		}
	}
}

func (jt *JobTimer) flush(currentTime int64) {
	for jt.priorityQueue.Count() > 0 {
		jobElement := jt.priorityQueue.Peek()
		if jobElement.execTick <= currentTime {

			if !jobElement.job.IsCancelled() {
				jobElement.job.Execute()
			}
			jt.priorityQueue.Pop() // Remove the executed job
		} else {
			break // Next job is not ready to execute
		}
	}
}

func (jt *JobTimer) Push(job IJob, tickAfter int) {
	if atomic.LoadInt32(&jt.shuttingDown) == 1 {
		return // Reject new jobs if shutting down
	}

	execTick := time.Now().UnixNano()
	if tickAfter > 0 {
		execTick += int64(tickAfter) * int64(time.Millisecond)
	}
	jt.priorityQueue.Push(JobTimerElement{execTick: execTick, job: job})
}

func (jt *JobTimer) Shutdown() {
	atomic.StoreInt32(&jt.shuttingDown, 1)
	close(jt.stop) // Send shutdown signal
}
