package scheduler

type IJob interface {
	Execute()
	IsCancelled() bool
	SetCancelled(cancel bool)
}

type Job struct {
	action    func()
	cancelled bool
}

func NewJob(action func()) *Job {
	return &Job{action: action}
}

func (j *Job) Execute() {
	if !j.cancelled {
		j.action()
	}
}

func (j *Job) IsCancelled() bool {
	return j.cancelled
}

func (j *Job) SetCancelled(cancel bool) {
	j.cancelled = cancel
}

type JobWithOneArg[T any] struct {
	action    func(T)
	arg1      T
	cancelled bool
}

func NewJobWithOneArg[T any](action func(T), arg1 T) *JobWithOneArg[T] {
	return &JobWithOneArg[T]{action: action, arg1: arg1}
}

func (j *JobWithOneArg[T]) Execute() {
	if !j.cancelled {
		j.action(j.arg1)
	}
}

func (j *JobWithOneArg[T]) IsCancelled() bool {
	return j.cancelled
}

func (j *JobWithOneArg[T]) SetCancelled(cancel bool) {
	j.cancelled = cancel
}

type JobWithTwoArgs[T1 any, T2 any] struct {
	action    func(T1, T2)
	arg1      T1
	arg2      T2
	cancelled bool
}

func NewJobWithTwoArgs[T1 any, T2 any](action func(T1, T2), arg1 T1, arg2 T2) *JobWithTwoArgs[T1, T2] {
	return &JobWithTwoArgs[T1, T2]{action: action, arg1: arg1, arg2: arg2}
}

func (j *JobWithTwoArgs[T1, T2]) Execute() {
	if !j.cancelled {
		j.action(j.arg1, j.arg2)
	}
}

func (j *JobWithTwoArgs[T1, T2]) IsCancelled() bool {
	return j.cancelled
}

func (j *JobWithTwoArgs[T1, T2]) SetCancelled(cancel bool) {
	j.cancelled = cancel
}

type JobWithThreeArgs[T1 any, T2 any, T3 any] struct {
	action    func(T1, T2, T3)
	arg1      T1
	arg2      T2
	arg3      T3
	cancelled bool
}

// NewJobWithThreeArgs creates a new JobWithThreeArgs.
func NewJobWithThreeArgs[T1 any, T2 any, T3 any](action func(T1, T2, T3), arg1 T1, arg2 T2, arg3 T3) *JobWithThreeArgs[T1, T2, T3] {
	return &JobWithThreeArgs[T1, T2, T3]{action: action, arg1: arg1, arg2: arg2, arg3: arg3}
}

// Execute runs the action of the job if it's not cancelled.
func (j *JobWithThreeArgs[T1, T2, T3]) Execute() {
	if !j.cancelled {
		j.action(j.arg1, j.arg2, j.arg3)
	}
}

// IsCancelled returns the cancellation status of the job.
func (j *JobWithThreeArgs[T1, T2, T3]) IsCancelled() bool {
	return j.cancelled
}

// SetCancelled sets the cancellation status of the job.
func (j *JobWithThreeArgs[T1, T2, T3]) SetCancelled(cancel bool) {
	j.cancelled = cancel
}
