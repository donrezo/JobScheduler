package scheduler

type Comparer[T any] interface {
	Less(other T) bool
}

type PriorityQueue[T Comparer[T]] struct {
	heap []T
}

// NewPriorityQueue creates a new priority queue.
func NewPriorityQueue[T Comparer[T]]() *PriorityQueue[T] {
	return &PriorityQueue[T]{heap: make([]T, 0)}
}

// Push adds an element to the priority queue.
func (pq *PriorityQueue[T]) Push(item T) {
	pq.heap = append(pq.heap, item)
	pq.upHeap()
}

// Pop removes and returns the highest priority element from the queue.
func (pq *PriorityQueue[T]) Pop() T {
	if len(pq.heap) == 0 {
		var zero T
		return zero
	}

	ret := pq.heap[0]
	last := pq.heap[len(pq.heap)-1]
	pq.heap = pq.heap[:len(pq.heap)-1]

	if len(pq.heap) > 0 {
		pq.heap[0] = last
		pq.downHeap()
	}

	return ret
}

// Peek returns the highest priority element without removing it.
func (pq *PriorityQueue[T]) Peek() T {
	if len(pq.heap) == 0 {
		var zero T
		return zero
	}

	return pq.heap[0]
}

func (pq *PriorityQueue[T]) upHeap() {
	idx := len(pq.heap) - 1
	for idx > 0 {
		parentIdx := (idx - 1) / 2
		if pq.heap[idx].Less(pq.heap[parentIdx]) {
			pq.heap[idx], pq.heap[parentIdx] = pq.heap[parentIdx], pq.heap[idx]
			idx = parentIdx
		} else {
			break
		}
	}
}

func (pq *PriorityQueue[T]) downHeap() {
	idx := 0
	lastIndex := len(pq.heap) - 1
	for {
		left := 2*idx + 1
		right := 2*idx + 2
		smallest := idx

		if left <= lastIndex && pq.heap[left].Less(pq.heap[smallest]) {
			smallest = left
		}

		if right <= lastIndex && pq.heap[right].Less(pq.heap[smallest]) {
			smallest = right
		}

		if smallest == idx {
			break
		}

		pq.heap[idx], pq.heap[smallest] = pq.heap[smallest], pq.heap[idx]
		idx = smallest
	}
}

func (pq *PriorityQueue[T]) Count() int {
	return len(pq.heap)
}
