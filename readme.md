# JobScheduler

JobScheduler is a Go package that provides a flexible job scheduling system. It allows you to schedule and execute jobs,
including those with arguments, at specified times or after specific intervals.

## Benchmark Results

| Benchmark                               | Iterations | Time per Operation | Memory per Operation | Allocations per Operation |
|-----------------------------------------|------------|--------------------|----------------------|---------------------------|
| BenchmarkFunctionExecutionPerSecond-12  | 100        | 10006878 ns/op     | 18 B/op              | 1 allocs/op               |
| BenchmarkFunctionPushPerSecond-12       | 2379198    | 502.5 ns/op        | 16 B/op              | 1 allocs/op               |
| BenchmarkMemoryUsage-12                 | 303138     | 4010 ns/op         | 1233 B/op            | 23 allocs/op              |

## Installation

To use JobScheduler in your Go project, you can add it as a dependency using Go modules. Make sure you have Go 1.20 or
later installed.

```bash
go get -u gitlab.com/donrezo/JobScheduler
```

## Usage

Import the scheduler package in your Go code to utilize the job scheduling functionality.

```go
import "gitlab.com/donrezo/JobScheduler/scheduler"
```

## Jobs
This structure ensures that jobs can be scheduled for later execution or executed immediately. The thread-safety - operation handled by channels.

```

      [JobScheduler]
       /         \
      /           \
(calls PushAfter) (calls Push)
    |               |
    |               |
[JobTimer]      [Queue]
    |               |
    |               | (Pop method)
    |               |
    |               v
    |               [Job]
    |             ^    ^    ^
    |             |    |    |
    |      (Implementations of IJob)
    |             |    |    |
    |---------------------------------
    |          |          |         |
 [Job]   [Job<T1>]  [Job<T1,T2>] [Job<T1,T2,T3>]
    |          |          |         |
 [Action] [Action<T1>] [Action<T1,T2>] [Action<T1,T2,T3>]
```

## Creating Jobs

You can create various types of jobs using the provided constructors. Here are some examples:

```go
// Create a job without arguments
job := scheduler.NewJob(func () {
// Your job logic here
})

// Create a job with one argument
jobWithArg := scheduler.NewJobWithOneArg(func (arg interface{}) {
// Your job logic with arg here
}, "example argument")

// Create a job with two arguments
jobWithTwoArgs := scheduler.NewJobWithTwoArgs(func (arg1, arg2 interface{}) {
// Your job logic with arg1 and arg2 here
}, "arg1", "arg2")

// Create a job with three arguments
jobWithThreeArgs := scheduler.NewJobWithThreeArgs(func (arg1, arg2, arg3 interface{}) {
// Your job logic with arg1, arg2, and arg3 here
}, "arg1", "arg2", "arg3")
```

## Using the Scheduler

The Scheduler type allows you to manage and execute jobs. Here's how to use it:

```go
// Create a new job scheduler
js := scheduler.NewJobScheduler(50) // 50 is the size of the job queue
defer jobScheduler.Shutdown() // Shutdown the scheduler when you're done with it
// Push a job to be executed immediately
js.Push(job)

// Push a job to be executed after a specified number of milliseconds
js.PushAfterJob(job, 1000) // Execute 'job' after 1000 milliseconds

// Flush the scheduler to execute pending jobs in the loop
//lock main thread
select {}
}
```

## Example

Check cmd/demo/main.go for a examples of how to use JobScheduler.

## Make

The provided Makefile offers a convenient set of commands to streamline your Go project development. Here are the available options:

* make build: Compile your project code and generate the jobscheduler binary.
* make test: Test the pkg and test directories with verbose output.
* make run: Execute your project (customize it for your specific needs).
* make cover: Calculate code coverage for pkg and test directories and generate an HTML report named coverage.html.
* make open-cover: Open the coverage report in your default web browser, providing visibility into code quality and test coverage.
* make benchmark: Run benchmark tests.
* make memprofile: Generate a memory profile from the benchmark tests.
* make open-memprofile: Open the memory profile in the default web browser.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

JobScheduler is licensed under the MIT license. See LICENSE for details. I dont take responsibility for any damage caused by this software. You use it at your own risk.
