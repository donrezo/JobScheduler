package main

import (
	"fmt"
	"gitlab.com/donrezo/JobScheduler/scheduler"
	"io"
	"net/http"
	"time"
)

func main() {

	jobScheduler := scheduler.NewJobScheduler(50)
	defer jobScheduler.Shutdown()
	// Schedule a simple job
	jobScheduler.PushAfterJob(func() {
		fmt.Printf("Simple job executed at Day: [%s] Hour: [%s]\n",
			time.Now().Format("01/02/2006"), // MM/DD/YYYY
			time.Now().Format("15:04:05"))   // HH:MM:SS
	}, 30000)

	// Schedule a job with one argument using interface and type assertion
	jobScheduler.PushAfterJobWithOneArg(func(arg interface{}) {
		fmt.Printf("Job 20: [%s] Hour: [%s] with arg: %s\n",
			time.Now().Format("01/02/2006"),
			time.Now().Format("15:04:05"),
			arg)
	}, "example argument", 20000)

	jobScheduler.PushAfterJobWithOneArg(func(arg interface{}) {
		fmt.Printf("Job 30: [%s] Hour: [%s] with arg: %s\n",
			time.Now().Format("01/02/2006"),
			time.Now().Format("15:04:05"),
			arg)
	}, "example argument", 30000)

	jobScheduler.PushAfterJobWithThreeArgs(func(arg, arg2, arg3 interface{}) {
		fmt.Printf("Job 10: [%s] Hour: [%s] with arg: %s,%s,%s\n",
			time.Now().Format("01/02/2006"),
			time.Now().Format("15:04:05"),
			arg, arg2, arg3)
	}, "example argument", 1, "dupa", 10000)

	jobScheduler.PushAfterJob(func() {
		httpClient := &http.Client{}
		req, err := http.NewRequest("GET", "https://www.google.com", nil)
		if err != nil {
			fmt.Println("Error creating request:", err)
			return
		}
		jobScheduler.PushAfterJob(func() {
			resp, err := httpClient.Do(req)
			if err != nil {
				fmt.Println("Error executing request:", err)
				return
			}
			defer resp.Body.Close()
			body, err := io.ReadAll(resp.Body)
			if err != nil {
				fmt.Println("Error reading response body:", err)
				return
			}
			jobScheduler.PushAfterJob(func() {
				fmt.Println(string(body))
				fmt.Printf("Inline executed at Day: [%s] Hour: [%s]\n",
					time.Now().Format("01/02/2006"), // MM/DD/YYYY
					time.Now().Format("15:04:05"))   // HH:MM:SS
			}, 6000)
		}, 6000)
		fmt.Printf("Curl executed at Day: [%s] Hour: [%s]\n",
			time.Now().Format("01/02/2006"), // MM/DD/YYYY
			time.Now().Format("15:04:05"))   // HH:MM:SS
	}, 10000)

	//block main thread
	select {}
}
