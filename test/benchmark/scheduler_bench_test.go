package benchmark

import (
	"fmt"
	scheduler2 "gitlab.com/donrezo/JobScheduler/scheduler"
	"testing"
	"time"
)

func BenchmarkFunctionExecutionPerSecond(b *testing.B) {
	jobScheduler := scheduler2.NewJobScheduler(10)
	defer jobScheduler.Shutdown()
	b.ResetTimer() // Start timing here

	for i := 0; i < b.N; i++ {
		jobScheduler.Push(scheduler2.NewJob(func() {
			// Function execution
		}))
		jobScheduler.Flush()
	}

	b.StopTimer() // Stop timing here
}

func BenchmarkFunctionPushPerSecond(b *testing.B) {
	s := scheduler2.NewJobScheduler(10)
	defer func() {
		b.StopTimer()
		s.Shutdown()
	}()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		s.Push(scheduler2.NewJob(func() {
			// Some minimal work to avoid complete optimization
			time.Sleep(1 * time.Nanosecond)
		}))
	}
}

func BenchmarkMemoryUsage(b *testing.B) {
	b.ReportAllocs() // Let the framework handle memory reporting
	for i := 0; i < b.N; i++ {
		s := scheduler2.NewJobScheduler(10)
		for j := 0; j < 5000000; j++ {
			s.PushAfterJob(func() { fmt.Println("dupa") }, 1000)
		}
		s.Flush()
		s.Shutdown()
	}
}
