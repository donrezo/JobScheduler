package scheduler

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/donrezo/JobScheduler/scheduler"
	"testing"
)

func TestNewJob(t *testing.T) {
	executed := false
	action := func() { executed = true }

	job := scheduler.NewJob(action)
	assert.False(t, job.IsCancelled(), "New job should not be cancelled")

	job.Execute()
	assert.True(t, executed, "Job action should have been executed")
}

func TestJobCancellation(t *testing.T) {
	job := scheduler.NewJob(func() {})
	assert.False(t, job.IsCancelled(), "New job should not be cancelled")

	job.SetCancelled(true)
	assert.True(t, job.IsCancelled(), "Job should be cancelled after calling SetCancelled")

	executed := false
	job = scheduler.NewJob(func() { executed = true })
	job.SetCancelled(true)
	job.Execute()
	assert.False(t, executed, "Cancelled job should not execute")
}

func TestNewJobWithOneArg(t *testing.T) {
	argPassed := 0
	action := func(arg int) { argPassed = arg }

	job := scheduler.NewJobWithOneArg(action, 42)
	assert.False(t, job.IsCancelled(), "New job with one arg should not be cancelled")

	job.Execute()
	assert.Equal(t, 42, argPassed, "Job with one arg should pass the correct argument")
}

func TestNewJobWithTwoArgs(t *testing.T) {
	arg1Passed, arg2Passed := 0, 0
	action := func(arg1, arg2 int) { arg1Passed = arg1; arg2Passed = arg2 }

	job := scheduler.NewJobWithTwoArgs(action, 42, 100)
	assert.False(t, job.IsCancelled(), "New job with two args should not be cancelled")

	job.Execute()
	assert.Equal(t, 42, arg1Passed, "Job with two args should pass the correct first argument")
	assert.Equal(t, 100, arg2Passed, "Job with two args should pass the correct second argument")
}

func TestNewJobWithThreeArgs(t *testing.T) {
	arg1Passed, arg2Passed, arg3Passed := 0, 0, 0
	action := func(arg1, arg2, arg3 int) { arg1Passed = arg1; arg2Passed = arg2; arg3Passed = arg3 }

	job := scheduler.NewJobWithThreeArgs(action, 42, 100, 200)
	assert.False(t, job.IsCancelled(), "New job with three args should not be cancelled")

	job.Execute()
	assert.Equal(t, 42, arg1Passed, "Job with three args should pass the correct first argument")
	assert.Equal(t, 100, arg2Passed, "Job with three args should pass the correct second argument")
	assert.Equal(t, 200, arg3Passed, "Job with three args should pass the correct third argument")
}
