package scheduler

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/donrezo/JobScheduler/scheduler"
	"sync"
	"testing"
	"time"
)

// MockJobTimer replaces the real JobTimer for testing purposes.
type MockJobTimer struct {
	mu         sync.Mutex
	jobs       []scheduler.IJob
	pushCalled bool
}

func NewMockJobTimer() *MockJobTimer {
	return &MockJobTimer{}
}

func (mjt *MockJobTimer) Push(job scheduler.IJob, tickAfter int) {
	mjt.mu.Lock()
	defer mjt.mu.Unlock()

	mjt.pushCalled = true
	mjt.jobs = append(mjt.jobs, job)
}

func (mjt *MockJobTimer) Flush() {
	mjt.mu.Lock()
	defer mjt.mu.Unlock()

	for _, job := range mjt.jobs {
		job.Execute()
	}
	mjt.jobs = nil
}

type MockJob struct {
	executed bool
}

func (mj *MockJob) Execute() {
	mj.executed = true
}

func (mj *MockJob) IsCancelled() bool {
	return false
}

func (mj *MockJob) SetCancelled(cancel bool) {
	mj.SetCancelled(cancel)
}

func TestSchedulerPushAndFlush(t *testing.T) {
	scheduler := scheduler.NewJobScheduler(50)
	mockJob := &MockJob{}
	scheduler.Push(mockJob)

	scheduler.Flush()

	assert.True(t, mockJob.executed, "Job should be executed after flush")
}

func TestNewJobScheduler(t *testing.T) {
	s := scheduler.NewJobScheduler(50)
	assert.NotNil(t, s)
}

func TestScheduler_PushAndFlush(t *testing.T) {
	var wg sync.WaitGroup
	s := scheduler.NewJobScheduler(50)
	jobExecuted := false

	wg.Add(1)
	s.Push(scheduler.NewJob(func() {
		jobExecuted = true
		wg.Done()
	}))

	s.Flush()

	wg.Wait()
	assert.True(t, jobExecuted, "Expected job to have been executed")
}

func TestScheduler_PushAfterJob(t *testing.T) {
	s := scheduler.NewJobScheduler(50)
	actionExecuted := false
	action := func() {
		actionExecuted = true
	}

	s.PushAfterJob(action, 0)

	time.Sleep(3 * time.Millisecond) // Allow some time for the job to be scheduled

	assert.True(t, actionExecuted, "Expected PushAfterJob action to be executed")
}

func TestScheduler_PushAfterJobWithOneArg(t *testing.T) {
	s := scheduler.NewJobScheduler(50)
	actionExecuted := false
	action := func(arg interface{}) {
		actionExecuted = true
		assert.Equal(t, "testArg", arg)
	}

	s.PushAfterJobWithOneArg(action, "testArg", 0)

	time.Sleep(1 * time.Millisecond)
	s.Flush()
	assert.True(t, actionExecuted, "Expected PushAfterJobWithOneArg action to be executed")
}

func TestScheduler_PushAfterJobWithTwoArgs(t *testing.T) {
	s := scheduler.NewJobScheduler(50)
	var arg1, arg2 string
	actionExecuted := false
	action := func(a1, a2 interface{}) {
		arg1, _ = a1.(string)
		arg2, _ = a2.(string)
		actionExecuted = true
	}

	s.PushAfterJobWithTwoArgs(action, "first", "second", 0)

	time.Sleep(3 * time.Millisecond) // Allow some time for the job to be scheduled
	s.Flush()
	assert.True(t, actionExecuted, "Expected PushAfterJobWithTwoArgs action to be executed")
	assert.Equal(t, "first", arg1, "Expected first argument to match")
	assert.Equal(t, "second", arg2, "Expected second argument to match")
}

func TestScheduler_PushAfterJobWithThreeArgs(t *testing.T) {
	s := scheduler.NewJobScheduler(50)
	var arg1, arg2, arg3 string
	actionExecuted := false
	action := func(a1, a2, a3 interface{}) {
		arg1, _ = a1.(string)
		arg2, _ = a2.(string)
		arg3, _ = a3.(string)
		actionExecuted = true
	}

	s.PushAfterJobWithThreeArgs(action, "first", "second", "third", 0)

	time.Sleep(1 * time.Millisecond) // Allow some time for the job to be scheduled
	s.Flush()
	assert.True(t, actionExecuted, "Expected PushAfterJobWithThreeArgs action to be executed")
	assert.Equal(t, "first", arg1, "Expected first argument to match")
	assert.Equal(t, "second", arg2, "Expected second argument to match")
	assert.Equal(t, "third", arg3, "Expected third argument to match")
}

func TestScheduler_JobCancellationDuringFlush(t *testing.T) {
	s := scheduler.NewJobScheduler(50)
	job := scheduler.NewJob(func() {})
	job.SetCancelled(true)
	s.Push(job)

	s.Flush()

	// Test that a cancelled job does not get executed
	assert.True(t, job.IsCancelled(), "Job should still be cancelled after flush")
}

func TestSchedulerShutdown(t *testing.T) {
	s := scheduler.NewJobScheduler(50)
	mockJob := &MockJob{}

	s.Shutdown() // Shutdown the scheduler

	s.Push(mockJob) // Try to push a job after shutdown

	s.Flush()

	// Assert that the job was not executed since the scheduler was shut down
	assert.False(t, mockJob.executed, "Job should not be executed after shutdown")
}
