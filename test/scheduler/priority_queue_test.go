package scheduler

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/donrezo/JobScheduler/scheduler"
	"testing"
)

// MockComparer is a simple implementation of the Comparer interface for testing.
type MockComparer struct {
	value int
}

func (mc MockComparer) Less(other MockComparer) bool {
	return mc.value < other.value
}

func TestNewPriorityQueue(t *testing.T) {
	pq := scheduler.NewPriorityQueue[MockComparer]()
	assert.NotNil(t, pq)
	assert.Equal(t, 0, pq.Count(), "New priority queue should be empty")
}

func TestPriorityQueue_PushPop(t *testing.T) {
	pq := scheduler.NewPriorityQueue[MockComparer]()
	pq.Push(MockComparer{value: 20})
	pq.Push(MockComparer{value: 30})
	pq.Push(MockComparer{value: 10})

	assert.Equal(t, 3, pq.Count(), "Priority queue should contain 3 items")

	first := pq.Pop()  // Should be 1
	second := pq.Pop() // Should be 3
	third := pq.Pop()  // Should be 2

	assert.Equal(t, 10, first.value, "The first item should be the smallest (highest priority)")
	assert.Equal(t, 20, second.value, "The second item should be the next smallest (next highest priority)")
	assert.Equal(t, 30, third.value, "The third item should be the largest (lowest priority)")
}

func TestPriorityQueue_Peek(t *testing.T) {
	pq := scheduler.NewPriorityQueue[MockComparer]()
	pq.Push(MockComparer{value: 2})
	pq.Push(MockComparer{value: 1}) // 1 has higher priority than 2

	top := pq.Peek()
	assert.Equal(t, 1, top.value, "Peek should return the smallest (highest priority) item without removing it")
	assert.Equal(t, 2, pq.Count(), "Peek should not remove the item")
}

func TestPriorityQueue_PopEmpty(t *testing.T) {
	pq := scheduler.NewPriorityQueue[MockComparer]()
	item := pq.Pop()

	// Default value of MockComparer should be returned for empty queue
	assert.Equal(t, 0, item.value, "Pop on an empty queue should return the zero value")
}

func TestPriorityQueue_Count(t *testing.T) {
	pq := scheduler.NewPriorityQueue[MockComparer]()
	assert.Equal(t, 0, pq.Count(), "Empty priority queue should have count of 0")

	pq.Push(MockComparer{value: 2})
	assert.Equal(t, 1, pq.Count(), "Priority queue should have count of 1 after one push")
}
