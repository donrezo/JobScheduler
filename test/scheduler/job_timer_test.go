package scheduler

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/donrezo/JobScheduler/scheduler"
	"testing"
	"time"
)

type MockTimeJob struct {
	Executed bool
}

func (mj *MockTimeJob) Execute() {
	mj.Executed = true
}

func (mj *MockTimeJob) IsCancelled() bool {
	return false
}

func (mj *MockTimeJob) SetCancelled(cancel bool) {
	mj.SetCancelled(cancel)
}

func TestJobTimer_PushAndFlush(t *testing.T) {
	jt := scheduler.NewJobTimer()
	job := &MockTimeJob{}

	// Push job to be executed immediately
	jt.Push(job, 0)

	// Allow some time for the job to be scheduled
	time.Sleep(1 * time.Millisecond)

	assert.True(t, job.Executed, "Job should be executed after flush")
}

func TestJobTimer_ScheduleFutureJob(t *testing.T) {
	jt := scheduler.NewJobTimer()
	job := &MockTimeJob{}

	// Push job to be executed after a delay
	jt.Push(job, 10) // 10 milliseconds

	// Flush should not execute the job yet
	assert.False(t, job.Executed, "Job should not be executed immediately after push")

	// Wait for more than 10 milliseconds
	time.Sleep(15 * time.Millisecond)

	// Now the job should be executed
	assert.True(t, job.Executed, "Job should be executed after enough time has passed")
}

func TestJobTimer_FlushMultipleJobs(t *testing.T) {
	jt := scheduler.NewJobTimer()
	job1 := &MockTimeJob{}
	job2 := &MockTimeJob{}

	// Push two jobs, one to be executed immediately, one after a delay
	jt.Push(job1, 0)
	jt.Push(job2, 10) // 10 milliseconds
	time.Sleep(1 * time.Millisecond)
	// Initially, only job1 should be executed
	assert.True(t, job1.Executed, "First job should be executed immediately")
	assert.False(t, job2.Executed, "Second job should not be executed immediately")

	// After a delay, job2 should also be executed
	time.Sleep(15 * time.Millisecond)
	assert.True(t, job2.Executed, "Second job should be executed after enough time has passed")
}
