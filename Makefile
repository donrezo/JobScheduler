GOCMD=go
GOBUILD=$(GOCMD) build
GORUN=$(GOCMD) run
GOTEST=$(GOCMD) test
BINARY_NAME=jobscheduler
COVERAGE_FILE=coverage.out
MEMPROFILE_FILE=memprofile.out

.PHONY: all build test run cover open-cover benchmark memprofile open-memprofile

all: test build

build:
	@$(GOBUILD) -o $(BINARY_NAME) -v ./cmd/jobscheduler

test:
	@$(GOTEST) -v ./pkg/... ./test/...

run:
	@$(GORUN) ./cmd/demo/main.go

cover:
	@$(GOTEST) -coverpkg=./scheduler/... -coverprofile=$(COVERAGE_FILE) ./scheduler/... ./test/...
	@$(GOCMD) tool cover -html=$(COVERAGE_FILE) -o coverage.html

open-cover: cover
	@echo "Opening coverage report in the default browser..."
	@open coverage.html

benchmark:
	@$(GOTEST) -bench=. -benchmem ./test/benchmark/...

memprofile:
	@$(GOTEST) -bench=. -benchmem -memprofile=$(MEMPROFILE_FILE) ./test/benchmark/...
	@echo "Memory profile saved to $(MEMPROFILE_FILE)"

open-memprofile: memprofile
	@echo "Opening memory profile in the default browser..."
	@$(GOCMD) tool pprof -http=:8080 $(MEMPROFILE_FILE)